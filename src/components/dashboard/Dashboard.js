import React, {Component} from 'react';
import {I18n} from 'react-i18next';
import Order from "../stateLess/Order";
import {setRenderFilter, RenderFilters} from "../../store/actions";
import {connect} from 'react-redux';
import * as $ from "jquery";
import {apiHost} from "../../App";
import axios from 'axios';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ordersToDisplay: 10,
            ordersPerRequest: 10,
            orders: [],
            showLoadMoreBtn: false
        }
        this.setOrders();
    }

    loadMoreOrders() {
        let newOrdersCount = this.state.ordersToDisplay + this.state.ordersPerRequest;
        this.setState(
            {
                ordersToDisplay: newOrdersCount
            },
            this.setOrders
        );
    }

    loadOrders(start, count) {
        let
            apiUrl = `${apiHost}verifier/api/v1/order/customer/list/${start}/${count}`,
            settings = {
                async: true,
                crossDomain: true,
                method: "GET",
                url: apiUrl
            };

        return axios(settings).then(res => res.data.data);
    }

    setOrders() {
        this.loadOrders(0, this.state.ordersToDisplay).then(data => {
            console.log(data);
            this.setState({
                orders: data
            });
        });
        /**
         * Check if we should show load more btn
         */
        this.loadOrders(this.state.ordersToDisplay, this.state.ordersPerRequest).then(data => {
            this.setState({
                showLoadMoreBtn: (data && data.length) ? true : false
            })
        })
    }

    render() {
        return (
            <I18n>
                {
                    (t) => (
                        <main>
                            <section className="create-task-container dashboard">
                                <button className="new-task"
                                        onClick={
                                            () => {
                                                window.location.pathname = "/templates";
                                            }
                                        }
                                >
                                    {t("templates.toTheConst")}
                                </button>
                                <div className="main-container">
                                    {
                                        this.state.orders.map((order) => (
                                            <Order
                                                orderName={order.orderName}
                                                status={order.status}
                                                orderComment={order.orderComment}
                                                verifAddr={order.verifAddr}
                                                orderId={order.orderId}
                                                orderRate={order.orderRate}
                                                orderRating={order.orderRating}
                                                orderCreatedAt={order.orderCreatedAt}
                                                key={order.orderId}
                                                userData={this.props.userData}
                                            />
                                        ))
                                    }
                                </div>
                                <div>
                                    { this.state.showLoadMoreBtn &&
                                        <div>
                                            <br/>
                                            <button className="load-more"
                                                    onClick={this.loadMoreOrders.bind(this)}
                                            >
                                                {t("common.loadMore")}
                                            </button>
                                            <br/>

                                            <br/>
                                        </div>
                                    }
                                    <br/>
                                </div>
                            </section>
                        </main>
                    )
                }
            </I18n>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        renderAppFilter: state.renderAppReducer
    }
};

const mapDispatchToProps = (dispatch) => {
    return ({
        fromDashToCont: () => {
            dispatch(setRenderFilter(RenderFilters.RENDER_CONSTRUCTOR))
        }
    })
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)