import React, {Component} from 'react';
import {I18n} from 'react-i18next';
import classSet from "react-classset";
import * as $ from 'jquery';
import {apiHost} from "../../App";
import axios from 'axios';
import OrderUserPhoto from '../stateLess/OrderUserPhoto';
import NotifyService from '../services/NotifyService';
import i18n from "../../i18n";

class PopUp extends Component {
    constructor(props) {
        super(props);
        this.notify = new NotifyService();

        this.state = {
            showApproveOrderBtn: this.props.status === 'VERIFIED',
            showEditOrderBtn: this.props.status === 'VERIFIED',
            showRemoveOrderBtn: this.props.status === 'CREATED',
            orderList: []
        };

        this.getValueArr = this.getValueArr.bind(this);
        this.ApprovePopup = this.ApprovePopup.bind(this);
        this.RemovePopup = this.RemovePopup.bind(this);
        this.EditPopup = this.EditPopup.bind(this);
        this.togglePopup = this.togglePopup.bind(this);
    }

    RemovePopup() {
        let message = i18n.t('popup.confirmationPrompt');
        this.notify.confirm(message)
            .then(() => {
                let fields = JSON.stringify({
                        orderId: this.props.orderId
                    }),
                    apiUrl = apiHost + "verifier/api/v1/order/delete",
                    settings = {
                        async: true,
                        crossDomain: true,
                        url: apiUrl,
                        method: "POST",
                        processData: false,
                        data: fields,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    };

                    console.log(fields);

                    axios(settings).then(response => {
                        this.notify.success(i18n.t('popup.removed'));
                        this.togglePopup();
                    }, err => {
                        this.notify.error(err.error);
                    });

            })
            .catch(() => {
                this.notify.log(i18n.t('popup.notConfirmed'));
                this.togglePopup();
            })
    }

    EditPopup() {
        let customTitle = i18n.t('popup.editPopupMessage');
        let defaultValue = i18n.t('popup.editPopupDefaultValue');

        this.notify.prompt(customTitle, '', defaultValue)
            .then((value) => {
                let editReason = value;

                let fields = JSON.stringify({
                        orderId: this.props.orderId,
                        reason: editReason
                    }),
                    apiUrl = apiHost + "verifier/api/v1/order/return",
                    settings = {
                        async: true,
                        crossDomain: true,
                        url: apiUrl,
                        method: "POST",
                        processData: false,
                        data: fields,
                        headers: {
                            "Content-Type": "application/json"
                        }
                    };

                axios(settings).then(response => {
                    this.notify.success(i18n.t('popup.returned'));
                    this.togglePopup();
                }, err => {
                    this.notify.error(err.response.data.error ? err.response.data.error : i18n.t('errors.default'));
                    this.togglePopup();
                });

            })
            .catch(() => {
                this.notify.log(i18n.t('popup.notConfirmed'));
            })
    }

    componentWillMount() {
        let apiUrl = apiHost + "verifier/api/v1/order/" + this.props.orderId,
            settings = {
                async: true,
                crossDomain: true,
                method: "GET",
                url: apiUrl
            };

        axios(settings).then((response) => {
            console.log(response);
            let orderPhotosArray = [];

            if (response.data.data && response.data.data.orderFields) {
                response.data.data.orderFields.map(field => {
                    if (field.fieldType == 'PHOTO') {
                        orderPhotosArray.push(field);
                    }
                })
            }
            
            this.setState({
                orderList: response.data.data,
                orderPhotos: orderPhotosArray,
                orderHasPhotos: orderPhotosArray.length ? true : false
            });
        });
    }


    ApprovePopup(){
        if (this.props.status === 'VERIFIED') {
            let fields = JSON.stringify({
                    orderId: this.props.orderId
                }),
                apiUrl = apiHost + "verifier/api/v1/order/approval",
                settings = {
                    async: true,
                    crossDomain: true,
                    url: apiUrl,
                    method: "POST",
                    processData: false,
                    data: fields,
                    headers: {
                        "Content-Type": "application/json"
                    }
                };

            axios(settings).then(response => {
                this.notify.success(i18n.t('popup.approved'));
                this.togglePopup();
            }, err => {
                this.notify.error(err.response.data.error ? err.response.data.error : i18n.t('errors.default'));
                this.togglePopup();
            });
        }
    }

    getValueArr() {
        let fieldsArray = this.state.orderList.orderFields;

        if (fieldsArray !== 'undefined') {
            $.map(fieldsArray, function(type) {
                return type.fieldName
            })
        }
    }

    togglePopup() {
        this.props.togglePopUp();
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.popUp !== this.props.popUp) {
            this.setState({popUp: nextProps.popUp});
        }
    }

    render() {
        const detailsToggle = classSet({
            'verification-details': true,
            'pop-up-active': this.props.popUp
        });

        return (
            <I18n>
                {
                    (t) => (

                        <section className={detailsToggle}>

                            <div className="details-box">
                                <div className="overlay">
                                    <div className="top"></div>
                                    <div className="bottom"></div>
                                </div>
                                <div className="content-scroll">

                                    <div className="close-icon" onClick={this.togglePopup}></div>
                                    <h2 className="details-name">{this.props.orderName}</h2>
                                    <span className="details-state">{this.props.status}</span>
                                    <p className="details-desc">{this.props.orderComment}</p>
                                    <div className="details-user-info">

                                        {/*Order user photo toggling condition goes here*/}
                                        {(this.state.orderHasPhotos) ? <OrderUserPhoto photos={this.state.orderPhotos} /> : null}

                                        <div className="user-info">
                                            {
                                                $.map(this.state.orderList.orderFields, (type, key) => {
                                                    if (type.fieldType === 'txt') return (
                                                        <div key={key}>
                                                            <span>{type.fieldName + ": "}</span>
                                                            <span>{type.fieldData}</span>
                                                        </div>
                                                    )
                                                })
                                            }
                                            <span className="details-city"><span>☻</span> {this.props.verifAddr}</span>

                                            <div className="pop-up-btns">
                                                {/*This one is visible only if order has status set to 'VERIFIED'*/}
                                                {  this.state.showApproveOrderBtn && <button
                                                    name="approveOrder"
                                                    id="approveOrder"
                                                    onClick={this.ApprovePopup}>{t("popup.approve")}</button>}

                                                { this.state.showRemoveOrderBtn && <button
                                                    name="removeOrder"
                                                    id="removeOrder"
                                                    onClick={this.RemovePopup}>{t("popup.remove")}</button> }

                                                { this.state.showEditOrderBtn && <button
                                                    name="editOrder"
                                                    id="editOrder"
                                                    onClick={this.EditPopup}>{t("popup.edit")}</button> }
                                                <br/>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>
                    )
                }
            </I18n>
        )
    }
}

export default PopUp;

