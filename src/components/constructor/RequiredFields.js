import React, {Component} from "react";
import {I18n} from 'react-i18next';
import AutoCompleteMap from "./Map";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.min.css';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import moment from 'moment';

export class RequiredFields extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        // console.log(props.firstName);
        this.state = {
            price: 1,
            f: props.templateData && props.templateData.firstName && props.templateData.lastName && props.templateData.middleName ? true : false,
            date: moment(),
            timeFrom:  moment('00:01', "HH:mm"),
            timeTo:  moment('23:59', "HH:mm"),
            firstName: props.templateData.firstName || '',
            lastName: props.templateData.lastName || '',
            middleName: props.templateData.middleName || ''
        };

        this.displayRateChange = this.displayRateChange.bind(this);
    }

    displayRateChange() {
        let value;
        try {
            value = document.getElementById("price").value;
        } catch (e) {
            value = 1;
        } finally {
            if (value !== this.state.price) {
                this.setState({
                    price: value
                });
            }
        }
    }

    onDateChange = date => this.setState({ date });

    onFromTimeChange = time => {
        console.log(time);
        this.setState({timeFrom: time})
    };

    onToTimeChange = time => this.setState({timeTo: time});

    componentDidUpdate() {
        this.displayRateChange();
    }

    render() {
        return (
            <I18n>
                {
                    (t) => (
                        <section className="constructor-required">
                            <h2 className="caption">{t("newTask.taskRequiredFields.caption")}</h2>
                            <div className="required-fields">
                                <div className="field-set names">
                                    <input
                                        id="f"
                                        name="f"
                                        type="checkbox"
                                        required={false}
                                        checked={this.state.f}
                                        value={this.state.f}
                                        onChange={() => {
                                            this.setState({
                                                f: !this.state.f
                                            });
                                        }}
                                    />
                                    <label htmlFor="f">
                                        <span className="ico"/>
                                        {t("newTask.taskRequiredFields.f")}
                                    </label>
                                    <input
                                        type="text"
                                        name="first-name"
                                        id="first-name"
                                        value={this.state.firstName}
                                        required={this.state.f}
                                        placeholder={t("newTask.taskRequiredFields.firstName")}
                                        disabled={!this.state.f}
                                        onChange={()=>{}}
                                    />
                                    <input
                                        type="text"
                                        name="last-name"
                                        id="last-name"
                                        value={this.state.lastName}
                                        required={this.state.f}
                                        placeholder={t("newTask.taskRequiredFields.lastName")}
                                        disabled={!this.state.f}
                                        onChange={()=>{}}
                                    />
                                    <input
                                        type="text"
                                        name="second-name"
                                        id="second-name"
                                        value={this.state.middleName}
                                        required={this.state.f}
                                        placeholder={t("newTask.taskRequiredFields.secondName")}
                                        disabled={!this.state.f}
                                        onChange={()=>{}}
                                    />
                                </div>
                                <div className="field-set date">
                                    <label htmlFor="date">{t("newTask.taskRequiredFields.date")}</label>
                                    <DatePicker
                                        id="date"
                                        name="date"
                                        dateFormat="YYYY-MM-DD"
                                        locale="ru-ru"
                                        todayButton={t("datePicker.todayLabel")}
                                        onChange={this.onDateChange}
                                        selected={this.state.date}
                                    />
                                    <label htmlFor="time">{t("newTask.taskRequiredFields.from")}</label>
                                    <TimePicker
                                        name="from"
                                        showSecond={false}
                                        style={{ width: 100 }}
                                        defaultValue={this.state.timeFrom}
                                        onChange={this.onFromTimeChange}
                                    />
                                    <label htmlFor="to">{t("newTask.taskRequiredFields.to")}</label>
                                    <TimePicker
                                        name="to"
                                        showSecond={false}
                                        style={{ width: 100 }}
                                        defaultValue={this.state.timeTo}
                                        onChange={this.onToTimeChange}
                                    />
                                </div>
                                <div className="field-set address">
                                    <label htmlFor="address">{t("newTask.taskRequiredFields.address")}</label>
                                    <input
                                        type="text"
                                        name="address"
                                        id="address"
                                        readOnly
                                        onFocus={(e) => {
                                            console.log(e.target);
                                            e.preventDefault();
                                            e.target.removeAttribute('readonly');
                                        }}
                                        required={true}
                                        placeholder={t("newTask.taskRequiredFields.addressPlaceholder")}
                                    />
                                    <input
                                        readOnly
                                        type="hidden"
                                        name="address_location_coords"
                                        id="address_location_coords"
                                        required={true}
                                    />
                                    <AutoCompleteMap/>
                                </div>
                                <div className="field-set price">
                                    <label htmlFor="price">
                                        {t("newTask.taskRequiredFields.price")}
                                        <span className="value">{this.state.price + "$"}</span>
                                    </label>
                                    {
                                        this.props.mount({
                                            type: "range",
                                            name: "price",
                                            id: "price",
                                            required: true,
                                            placeholder: "pricePlaceholder",
                                            min: "1",
                                            max: "1000",
                                            step: "1",
                                            value: this.state.price,
                                            onChange: this.displayRateChange
                                        })
                                    }
                                </div>
                                <div className="field-set comment">
                                    {
                                        this.props.mount({
                                            name: "comment",
                                            id: "comment",
                                            required: true,
                                            rows: "5",
                                            placeholder: t("newTask.taskRequiredFields.comment")
                                        })
                                    }
                                </div>
                                <div className="field-set buttons">
                                    <button
                                        type="button"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            this.props.onToggle();
                                        }}>
                                        {t("newTask.taskRequiredFields.preview")}
                                    </button>
                                    <button
                                        type="button"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            this.props.saveConstructorAsTemplate();
                                        }}>
                                        {t("newTask.taskRequiredFields.save")}
                                    </button>
                                </div>
                            </div>
                            <input
                                type="submit"
                                name="constructor-submit"
                                id="constructor-submit"
                            />
                            <label htmlFor="constructor-submit">{t("newTask.submit")}</label>
                        </section>
                    )
                }
            </I18n>
        );
    }
}