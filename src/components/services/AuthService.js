import CookieService from './CookieService';

class AuthService{
    constructor() {
        this.cookieService = new CookieService();
    }

    getToken() {
        return  document.cookie.includes('token') ? this.cookieService.getCookie('token') : "";
    };
}

export default AuthService