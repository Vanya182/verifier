import 'alertifyjs/build/css/alertify.min.css';
import 'alertifyjs/build/css/themes/semantic.min.css';
import Alertify from 'alertifyjs';
import i18n from "../../i18n";

class NotifyService {
    constructor() {

    }

    success(message) {
        Alertify.success(message);
    }

    error(message) {
        Alertify.error(message);
    }

    log(message) {
        Alertify.message(message);
    }

    prompt(customTitle, message, defaultValue) {
        return new Promise((resolve, reject) => {
            Alertify.prompt(customTitle, message, defaultValue,
            function(evt, value ){
                resolve(value);
            },
            function(){
                reject();
            });
        })
    }

   confirm(message) {
        return new Promise((resolve, reject) => {
            Alertify.confirm(i18n.t('popup.confirmDialogTitle'), message,
            function(){
                resolve();
            },
            function(){
                reject();
            });
        })

   }

}

export default NotifyService