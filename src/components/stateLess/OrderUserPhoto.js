import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import $ from 'jquery';

class OrderUserPhoto extends Component {
    constructor(props) {
        super(props);
        this.photosArray = props.photos;
        
    }
    componentWillMount() {
        let delimeter = ',';
        let photos = [];
        this.photosArray.map(item => {
            let rawPhotos = item.fieldData.split(delimeter);
            photos = photos.concat(rawPhotos);
        });
        this.setState({
            photos
        })
    }
    componentDidMount() {
        window.dispatchEvent(new Event('resize'));
    }

    render() {
        let settings = {
            dots: true,
            arrows: false,
            infinite: false,
            dragable: false,
            speed: 0,
            slidesToShow: 1,
            autoplay: false,
            slidesToScroll: 1
        };


        return (
            <div className="user-photo">
                <Slider {...settings}>
                    {
                        this.state.photos.map(photo => {
                        return (
                        <div key={photo}>
                            <div className="img-box" style={
                                {
                                    backgroundImage: 'url(' + photo + ')'
                                }
                            }></div>
                        </div>
                        )})
                    }
                </Slider>
            </div>
        )
    }
}

OrderUserPhoto.propTypes = {
    // orderName: PropTypes.string.isRequired,
    // status: PropTypes.string.isRequired,
    // orderComment: PropTypes.string.isRequired,
    // verifAddr: PropTypes.string.isRequired,
    // orderId: PropTypes.number.isRequired,
    // orderRate: PropTypes.number.isRequired,
    // orderRating: PropTypes.number.isRequired,
    // orderCreatedAt: PropTypes.number.isRequired
};

export default OrderUserPhoto;