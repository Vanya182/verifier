import axios from 'axios';
import AuthService from '../services/AuthService';

class AxiosInterceptor{
    constructor() {
        this.authService = new AuthService();
        let token = this.authService.getToken();
        axios.interceptors.request.use(function (config) {
            if (config) {
                config.headers.Token = token
            }
            return config;
        }, function (error) {
            return Promise.reject(error.response);
        });
    }
}

export default AxiosInterceptor;